Boot performance test

Runs systemd-analyze and system-analyze --blame.
The results are not exploited at this time, they may need to be compared
to platform specific references (https://phabricator.apertis.org/T4817).
