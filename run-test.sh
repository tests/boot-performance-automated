#!/bin/sh
# This script will execute all the systemd-analyze tests

echo "Running test systemd-analyze time ..."

TIME=`systemd-analyze time`
echo $TIME
for var in ${TIME}; do
	ACTION=""
	case $var in
	\(kernel\)) ACTION="(kernel)";;
	\(firmware\)) ACTION="(firmware)";;
	\(loader\)) ACTION="(loader)";;
	\(userspace\)) ACTION="(userspace)";;
	*) DELAY=${var};;
	esac
	if [ -n "${ACTION}" -a -n "${DELAY}" ] ; then
		echo "${ACTION}\t${DELAY}"
	fi
done

echo "Running test systemd-analyze blame ..."

systemd-analyze blame | while read DELAY ACTION ; do
	echo "(${ACTION})\t${DELAY}"
done

